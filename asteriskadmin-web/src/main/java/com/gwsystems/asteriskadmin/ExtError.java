package com.gwsystems.asteriskadmin;

/**
 * Created by paalgyula on 2013.10.24. with IntelliJ IDEA
 */
public class ExtError {
    private boolean success = false;
    private String errorMessage;

    public ExtError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
