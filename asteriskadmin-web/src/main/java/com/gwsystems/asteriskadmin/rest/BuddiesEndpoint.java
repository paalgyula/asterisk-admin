package com.gwsystems.asteriskadmin.rest;

import com.gwsystems.asteriskadmin.ExtPagedResult;
import com.gwsystems.asteriskadmin.ejb.SipBuddiesService;

import javax.ejb.EJB;
import javax.ws.rs.*;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Path("/buddies")
public class BuddiesEndpoint {

    @EJB
    private SipBuddiesService sipBuddiesService;

    @POST
    @Path("/add")
    public void addBuddy() {

    }

    @GET
    @Produces("application/json")
    public ExtPagedResult listBuddies() {
        ExtPagedResult result = new ExtPagedResult();
        result.setTotal(sipBuddiesService.getBuddiesCount());
        result.setValues(sipBuddiesService.getBuddies());

        return result;
    }

    @POST
    @Produces
    @Path("/del/{id}")
    public ExtPagedResult deleteBuddy(@PathParam("id") Long id) {
        sipBuddiesService.delBuddy(id);
        return new ExtPagedResult();
    }
}
