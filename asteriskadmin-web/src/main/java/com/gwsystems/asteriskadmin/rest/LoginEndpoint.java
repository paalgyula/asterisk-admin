package com.gwsystems.asteriskadmin.rest;

import com.gwsystems.asteriskadmin.ExtError;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

/**
 * Created by paalgyula on 2013.10.24. with IntelliJ IDEA
 */
@Path("/login")
public class LoginEndpoint {
    @Context
    private HttpServletRequest servletRequest;

    @POST
    @Produces("application/json")
    public ExtError login(@FormParam("j_username") String username, @FormParam("j_password") String password) {
        ExtError extError = new ExtError("");
        try {
            servletRequest.login(username, password);
            extError.setSuccess(true);
        } catch (Exception e) {
            extError.setErrorMessage("Felhasználónév vagy jelszó nem megfelelő!");
            extError.setSuccess(false);
        }

        return extError;
    }
}
