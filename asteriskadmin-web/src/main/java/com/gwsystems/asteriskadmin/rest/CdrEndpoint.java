package com.gwsystems.asteriskadmin.rest;

import com.gwsystems.asteriskadmin.ExtPagedResult;
import com.gwsystems.asteriskadmin.ejb.CallDestinationRecordService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2013.10.27.
 * Time: 15:03
 */
@Path("/cdr")
@RequestScoped
public class CdrEndpoint {
    @EJB
    private CallDestinationRecordService callDestinationRecordService;

    @POST
    @Produces("application/json")
    public ExtPagedResult getCdrList(
            @FormParam("start")
            @DefaultValue("0")
            int start,

            @FormParam("limit")
            @DefaultValue("25")
            int limit,

            @FormParam("source")
            String source
    ) {
        ExtPagedResult result = new ExtPagedResult();
        result.setSuccess(true);
        result.setValues(callDestinationRecordService.getCdrForBuddy(source, limit, start));

        return result;
    }
}
