package com.gwsystems.asteriskadmin.rest;

import com.gwsystems.asteriskadmin.ExtPagedResult;
import com.gwsystems.asteriskadmin.ejb.ContextService;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by paalgyula on 2013.10.24. with IntelliJ IDEA
 */
@Path("/contexts")
public class ContextEndpoint {
    //$result = mysql_query( 'SELECT id, context, exten, priority, app, appData FROM extensions;' );
    @EJB
    private ContextService contextService;

    @GET
    @Produces("application/json")
    public ExtPagedResult getContexts() {
        ExtPagedResult pagedResult = new ExtPagedResult();
        pagedResult.setTotal(50);
        pagedResult.setValues(contextService.getExtensions());
        return pagedResult;
    }

    @GET
    @POST
    @Produces("application/json")
    @Path("/names")
    public ExtPagedResult getExtensionNames() {
        ExtPagedResult pagedResult = new ExtPagedResult();
        List<Map<String, String>> nameMapList = new ArrayList<Map<String, String>>();
        for (String name : contextService.getExtensionNames()) {
            Map<String, String> nameValueMap = new HashMap<String, String>();
            nameValueMap.put("name", name);
            nameMapList.add(nameValueMap);
        }

        pagedResult.setValues(nameMapList);
        return pagedResult;
    }

}
