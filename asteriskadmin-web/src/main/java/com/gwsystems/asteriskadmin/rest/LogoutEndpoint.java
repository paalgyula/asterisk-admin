package com.gwsystems.asteriskadmin.rest;

import com.gwsystems.asteriskadmin.ExtError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

/**
 * Created by paalgyula on 2013.10.24. with IntelliJ IDEA
 */
@Path("/logout")
public class LogoutEndpoint {
    @Context
    private HttpServletRequest httpServletRequest;

    @POST
    @Produces("application/json")
    public ExtError logout() {
        ExtError extError = new ExtError("");

        try {
            httpServletRequest.logout();
            extError.setSuccess(true);
        } catch (ServletException e) {
            e.printStackTrace();
            extError.setSuccess(false);
            extError.setErrorMessage(e.getMessage());
        }

        return extError;
    }
}
