package com.gwsystems.asteriskadmin;

import java.util.List;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
public class ExtPagedResult {
    private long total;
    private int page;
    private boolean success = true;
    private List<?> values;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<?> getValues() {
        return values;
    }

    public void setValues(List<?> values) {
        this.values = values;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
