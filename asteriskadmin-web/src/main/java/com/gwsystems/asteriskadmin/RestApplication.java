package com.gwsystems.asteriskadmin;

import com.gwsystems.asteriskadmin.rest.*;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by paalgyula on 2013.10.20. with IntelliJ IDEA
 */
@ApplicationPath("/ajax")
public class RestApplication extends Application {
    private Set<Object> singletons = new HashSet<Object>();
    private Set<Class<?>> classes = new HashSet<Class<?>>();

    public RestApplication() {
        classes.add(BuddiesEndpoint.class);
        classes.add(LoginEndpoint.class);
        classes.add(LogoutEndpoint.class);
        classes.add(ContextEndpoint.class);
        classes.add(CdrEndpoint.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}

