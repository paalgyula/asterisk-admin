Ext.onReady(function () {

    Ext.QuickTips.init();

    var loginButton = new Ext.Button({
        text: 'Login',
        formBind: true,
        handler: function () {
            loginFormPanel.getForm().submit({
                method: 'POST',
                waitTitle: 'Login',
                waitMsg: 'Beléptetés folyamatban...',
                success: function () {
                    var redirect = 'main.html';
                    window.location = redirect;
                },
                failure: function (form, action) {
                    if (action.failureType == 'server') {
                        var obj = Ext.util.JSON.decode(action.response.responseText);
                        Ext.Msg.alert('Login Failed!', obj.errorMessage);
                    } else {
                        Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText);
                    }
                    loginFormPanel.getForm().reset();
                }
            });
        }
    })

    var loginFormPanel = new Ext.FormPanel({
        labelWidth: 140,
        width: 320,
        height: 95,
        url: 'ajax/login',
        frame: true,
        defaultType: 'textfield',
        monitorValid: true,
        defaults: {
            blankText: 'A mezőt kötelező kitölteni!',
            allowBlank: false,
            enableKeyEvents: true,
            listeners: {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER) {
                        loginButton.handler.call(loginButton.scope, loginButton, Ext.EventObject);
                    }
                }
            }
        },
        items: [
            {
                fieldLabel: 'Felhasználónév',
                name: 'j_username'
            },
            {
                fieldLabel: 'Jelszó',
                name: 'j_password',
                inputType: 'password'
            }
        ],
        buttons: [loginButton]
    });


    var win = new Ext.Window({
        layout: 'fit',
        title: 'Login form',
        closable: false,
        resizable: false,
        border: true,
        items: [loginFormPanel]
    });

    win.show();
});