Ext.onReady(function () {

    var jsonCdrReader = new Ext.data.JsonReader({
        root: 'values',
        totalProperty: 'total'
    }, [
        {name: 'clid', type: 'string', mapping: 'clid'},
        {name: 'calldate', type: 'string', mapping: 'calldate'},
        {name: 'src', type: 'string', mapping: 'src'},
        {name: 'dst', type: 'string', mapping: 'dst'},
        {name: 'billsec', type: 'string', mapping: 'billsec'},
        {name: 'disposition', type: 'string', mapping: 'disposition'},
    ]);

    cdrDataStore = new Ext.data.Store({
        sortInfo: { field: 'calldate', direction: 'DESC' },
        reader: jsonCdrReader,
        proxy: new Ext.data.HttpProxy({
            url: 'ajax/cdrGet.php'
        })
    });

    cdrDataStore.load();

    cdrColModel = new Ext.grid.ColumnModel([
        {
            header: "Hívó azonosító",
            dataIndex: 'clid',
            sortable: true,
            width: 200
        },
        {
            header: 'Hívás kezdetének ideje:',
            dataIndex: 'calldate',
            sortable: true,
            width: 130
        },
        {
            header: 'Hívó',
            dataIndex: 'src',
            sortable: true,
            width: 140
        },
        {
            header: 'Hívott',
            dataIndex: 'dst',
            sortable: true,
            width: 140
        },
        {
            header: 'Számlázott idő (másodperc)',
            dataIndex: 'billsec',
            sortable: true,
            width: 150
        },
        {
            header: 'DISP',
            dataIndex: 'disposition',
            sortable: true,
            width: 200
        }
    ]);

    cdrGrid = new Ext.grid.EditorGridPanel({
        colModel: cdrColModel,
        store: cdrDataStore,
        remoteSort: true,
        selModel: new Ext.grid.RowSelectionModel,
        bbar: new Ext.PagingToolbar({
            pageSize: 25,
            store: cdrDataStore,
            displayInfo: true,
            displayMsg: 'Displaying contexts {0} - {1} of {2}'
        }),
        tbar: new Ext.PagingToolbar({
            pageSize: 25,
            store: cdrDataStore,
            displayInfo: true,
            displayMsg: 'Displaying contexts {0} - {1} of {2}'
        })
    });
});
