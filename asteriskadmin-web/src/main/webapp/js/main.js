Ext.onReady(function () {

    Ext.QuickTips.init();

    var peerManager = new PeerManager();

    var toolBar = new Ext.Toolbar({
        items: [
            '<b>Üdvözöljük az Asterisk adminisztrációs panelen!</b>',
            '->',
            '-',
            {
                xtype: 'button',
                text: 'Kilépés',
                icon: 'resources/images/icons/control_power_blue.png',
                iconCls: 'icon-exit',
                handler: function () {
                    Ext.Ajax.request({
                        url: 'ajax/logout',
                        method: 'POST',
                        waitTitle: 'Kijelentkezés',
                        waitMsg: 'Kijelentkeztetés folyamatban',
                        success: function () {
                            window.location.href = 'index.html';
                        }
                    });
                }
            }
        ]
    });

    new Ext.Viewport({
        layout: 'fit',
        items: {
            tbar: toolBar,
            layout: 'border',
            items: [
                {region: 'south', height: 100, split: true},
                {
                    region: 'center',
                    layout: 'fit',
                    items: [
                        window.tabPanel = new Ext.TabPanel({
                            activeTab: 0,
                            items: [
                                {
                                    title: 'Asterisk Peerek',
                                    iconCls: 'icon-asterisk',
                                    layout: 'fit',
                                    border: true,
                                    items: [peerManager.getGrid()]
                                },
                                {
                                    title: 'Context',
                                    iconCls: 'icon-context',
                                    layout: 'fit',
                                    border: true,
                                    items: [
                                        contextGrid
                                    ]
                                },
                                {
                                    title: 'Híváslista',
                                    iconCls: 'icon-context',
                                    layout: 'fit',
                                    border: true,
                                    items: [
                                        cdrGrid
                                    ]
                                }
                            ]
                        })
                    ]
                }
            ]
        }
    });

});