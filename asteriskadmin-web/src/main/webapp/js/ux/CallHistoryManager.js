/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2013.10.27.
 * Time: 13:39
 * To change this template use File | Settings | File Templates.
 */
Ext.ux.CallHistoryManager = Ext.extend(Ext.grid.GridPanel, {
    constructor: function (config) {

        var myPageSize = 50;
        var source = config.source;

        var secondsToTime = function (value) {
            var sec_num = parseInt(value, 10); // don't forget the second parm
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            var time = hours + ':' + minutes + ':' + seconds;
            return time;
        }

//      Create configuration for this Grid.
        var store = new Ext.data.JsonStore({
            baseParams: {
                source: source
            },
            autoLoad: {params: {start: 0, limit: myPageSize}},
            proxy: new Ext.data.HttpProxy({
                url: 'ajax/cdr'
            }),
            idProperty: 'uniqueid',
            root: 'values',
            fields: [
                {
                    name: 'id'
                },
                {
                    name: 'dst'
                },
                {
                    name: 'calldate'
                },
                {
                    name: 'billsec'
                },
                {
                    name: 'disposition'
                },
                {
                    name: 'duration'
                },
                {
                    name: 'uniqueid'
                }
            ]
        });

        var bottomBar = new Ext.PagingToolbar({
            store: store,
            displayInfo: true,
            pageSize: myPageSize,
            prependButtons: true,
            items: [
                'text 1'
            ]
        })

        var colModel = new Ext.grid.ColumnModel({
            columns: [
                new Ext.grid.RowNumberer(),
                {
                    header: "Hívott fél telefonszáma",
                    mapping: 'dst',
                    width: 180,
                    menuDisabled: false,
                    render: function (value) {
                        if (value.match(/06........./))
                            return "<b>" + value + "</b>";
                        else
                            return value;
                    }
                },
                {
                    header: "Hívás ideje",
                    mapping: 'calldate',
                    width: 180,
                    sortable: true,
                    menuDisabled: false,
                    renderer: function (value) {
                        return new Date(value).format('Y-m-d G:i:s');
                    }
                },
                {
                    header: 'Hívás hossza',
                    mapping: 'billsec',
                    sortable: true,
                    renderer: function (value) {
                        return secondsToTime(value);
                    }
                },
                {
                    header: 'Hívás állapota',
                    mapping: 'disposition',
                    renderer: function (value) {
                        if (value == "ANSWERED")
                            return "<img src=\"resources/icons/phone_green.png\" alt=\"Megválaszolt\" title=\"Megválaszolt\"/> Megválaszolt";
                        else
                            return "<img src=\"resources/icons/phone_red.png\" alt=\"Sikertelen\" title=\"Sikertelen\"/> Sikertelen";
                    }
                },
                {
                    header: 'Kommunikáció hossza',
                    mapping: 'duration',
                    renderer: function (value) {
                        return secondsToTime(value);
                    }
                }
            ],
            defaults: {
                sortable: true,
                menuDisabled: true,
                width: 100
            },
            listeners: {
                hiddenchange: function (cm, colIndex, hidden) {
                    saveConfig(colIndex, hidden);
                }
            }
        });

//      Create a new config object containing our computed properties
//      *plus* whatever was in the config parameter.
        config = Ext.apply({
            store: store,
            colModel: colModel,
            bbar: bottomBar
        }, config);

        Ext.ux.CallHistoryManager.superclass.constructor.call(this, config);

//      Your postprocessing here
    }
});