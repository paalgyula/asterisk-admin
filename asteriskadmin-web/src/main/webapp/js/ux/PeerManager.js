PeerManager = function () {
    var peerManager = this;

    this.contextStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({url: 'ajax/contexts/names'}),
        reader: new Ext.data.JsonReader({
            root: 'values',
            fields: [
                {name: 'name'}
            ]
        })
    });

    this.newPeerForm = new Ext.FormPanel({
        frame: true,
        scope: this,
        labelWidth: 130,
        monitorValid: true,
        defaults: {
            allowBlank: false,
            blankText: "A mező kitöltése kötelező"
        },
        buttonAlign: 'center',
        defaultType: 'textfield',
        items: [
            {
                width: 200,
                fieldLabel: 'Telefonszám (user)',
                id: 'name',
                emptyText: '2024754'
            },
            {
                width: 200,
                allowBlank: true,
                fieldLabel: 'Hívó azonosító',
                id: 'callerid',
                emptyText: 'Goofy a hegyről'
            },
            {
                width: 200,
                xtype: 'combo',
                fieldLabel: 'Context',
                id: 'context',
                forceSelection: true,
                editable: false,
                allowBlank: false,
                triggerAction: 'all',
                store: peerManager.contextStore,
                displayField: 'name',
                valueField: 'name',
                mode: 'remote',
                emptyText: 'context'
            },
            {
                width: 200,
                fieldLabel: 'Jelszó',
                emptyText: 'asd123',
                allowBlank: true,
                id: 'secure'
            },
            {
                width: 200,
                fieldLabel: 'Host',
                id: 'host',
                emptyText: '(dynamic/voipblast.com)'
            },
            {
                width: 200,
                xtype: 'combo',
                fieldLabel: 'NAT',
                triggerAction: 'all',
                lazyRender: true,
                forceSelection: true,
                editable: false,
                id: 'nat',
                triggerAction: 'all',
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'id',
                        'value'
                    ],
                    data: [
                        ['yes', 'Igen'],
                        ['no', 'Nem']
                    ]
                }),
                valueField: 'id',
                displayField: 'value'
            },
            {
                width: 200,
                xtype: 'combo',
                fieldLabel: 'Típus',
                id: 'type',
                mode: 'local',
                forceSelection: true,
                editable: false,
                triggerAction: 'all',
                lazyRender: true,
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'id',
                        'value'
                    ],
                    data: [
                        ['friend', 'Friend'],
                        ['peer', 'Peer']
                    ]
                }),
                valueField: 'id',
                displayField: 'value'
            }
        ],
        buttons: [
            {
                text: 'Felvétel',
                iconCls: 'icon-add',
                formBind: true,
                scope: this,
                handler: function () {
                    peerManager.newPeerForm.getForm().submit({
                        url: 'ajax/buddies/add',
                        waitMsg: 'Új eszköz hozzáadása...',
                        failure: function (frm, act) {
                            Ext.MessageBox.alert('Hiba', 'Ismeretlen hiba miatt az eszköz nem került felvételre');
                        },
                        success: function (frm, act) {
                            peerManager.newPeerWin.hide();
                            peerManager.ds.reload();
                        }
                    });
                }
            },
            {
                text: 'Mégsem',
                handler: function () {
                    peerManager.newPeerForm.getForm().reset();
                    peerManager.newPeerWin.hide();
                }
            },
            {
                text: 'Reset',
                handler: function () {
                    peerManager.newPeerForm.getForm().reset();
                }
            }
        ]
    });

    this.newPeerWin = new Ext.Window({
        width: 370,
        modal: true,
        resizable: false,
        draggable: true,
        closable: false,
        title: 'Új peer',
        items: [peerManager.newPeerForm]
    });

    this.cm = new Ext.grid.ColumnModel([
        {
            header: "ID",
            dataIndex: 'id',
            width: 40,
            sortable: true,
            hidden: true
        },
        {
            header: "Telefonszám",
            dataIndex: 'name',
            width: 150,
            sortable: true,
            editor: new Ext.form.NumberField({
                allowBlank: false,
                maxLength: 20,
                sortable: true,
                width: 150
            })
        },
        {
            header: "Online",
            dataIndex: 'online',
            width: 50,
            sortable: false,
            renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                if (value == "online")
                    return '<img class="online" src="resources/icons/accept.png"/>';
                else
                    return '<img class="online" src="resources/icons/delete.png"/>';
            }
        },
        {
            header: "Hívó azonosító",
            dataIndex: 'callerid',
            width: 200,
            sortable: true,
            editor: new Ext.form.TextField({
                allowBlank: false,
                maxLength: 50,
                width: 200
            })
        },
        {
            header: "Context",
            dataIndex: 'context',
            width: 100,
            sortable: true,
            editor: new Ext.form.ComboBox({
                width: 100,
                xtype: 'combo',
                fieldLabel: 'Context',
                id: 'context',
                forceSelection: true,
                editable: false,
                allowBlank: false,
                triggerAction: 'all',
                store: peerManager.contextStore,
                displayField: 'name',
                valueField: 'name',
                mode: 'remote',
                emptyText: 'context'
            })
        },
        {
            header: "Jelszó",
            dataIndex: 'secret',
            width: 150,
            sortable: true,
            renderer: function (val) {
                return val.replace(/./g, '●');
            },
            editor: new Ext.form.TextField({
                allowBlank: false,
                maxLength: 50,
                width: 150
            })
        },
        {
            header: "Host",
            dataIndex: 'host',
            width: 150,
            sortable: true,
            editor: new Ext.form.TextField({
                allowBlank: false,
                maxLength: 100,
                width: 150
            })
        },
        {
            header: "NAT",
            dataIndex: 'nat',
            width: 80,
            sortable: true,
            renderer: function (val) {
                return (val == "yes") ? "Igen" : "Nem";
            },
            editor: new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'id',
                        'value'
                    ],
                    data: [
                        ['yes', 'Igen'],
                        ['no', 'Nem']
                    ]
                }),
                valueField: 'id',
                displayField: 'value'
            })
        },
        {
            header: "Típus",
            dataIndex: 'type',
            width: 150,
            sortable: true,
            editor: new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                // transform the data already specified in html
                transform: 'peerType',
                lazyRender: true,
                listClass: 'x-combo-list-small'
            })
        },
        {
            xtype: 'actioncolumn',
            header: 'Műveletek',
            width: 200,
            sortable: false,
            items: [
                {
                    icon: 'resources/icons/telephone_bw.png',
                    tooltip: 'Híváslista',
                    handler: function (grid, rowIndex, colIndex) {
                        var rec = peerManager.ds.getAt(rowIndex);
                        var peerId = rec.get('id');
                        var callerId = rec.get('callerid');
                        var tab = window.tabPanel.add(new Ext.ux.CallHistoryManager({
                            icon: 'resources/icons/telephone_bw.png',
                            title: '<b>' + callerId + '</b> híváslistája',
                            closable: true,
                            source: rec.get('name')
                        }));
                        // Aktivaljuk a tab-ot
                        window.tabPanel.setActiveTab(tab.id);
                    }
                }
            ]
        }
    ]);


    this.jsonPeerReader = new Ext.data.JsonReader({
        root: 'values',
        totalProperty: 'total',
        id: 'id'
    }, [
        {name: 'id', type: 'int', mapping: 'id'},
        {name: 'name', type: 'int', mapping: 'name'},
        {name: 'callerid', type: 'string', mapping: 'callerid'},
        {name: 'context', type: 'string', mapping: 'context'},
        {name: 'secret', type: 'string', mapping: 'secret'},
        {name: 'host', type: 'string', mapping: 'host'},
        {name: 'nat', type: 'string', mapping: 'nat'},
        {name: 'type', type: 'string', mapping: 'type'},
        {name: 'online', type: 'string', mapping: 'online'}
    ]);

    this.ds = new Ext.data.Store({
        reader: peerManager.jsonPeerReader,
        sortInfo: { field: 'name', direction: 'ASC' },
        proxy: new Ext.data.HttpProxy({
            url: 'ajax/buddies?ac=showData' //a function in your php-script must be activated when ac = showdata
        })
    });

    this.grid = new Ext.grid.EditorGridPanel({
        colModel: peerManager.cm,
        ds: peerManager.ds,
        selModel: new Ext.grid.RowSelectionModel,
        bbar: [new Ext.PagingToolbar({
            store: peerManager.ds
        })],
        tbar: [
            {
                xtype: 'button',
                text: 'Új Peer',
                iconCls: 'icon-add',
                handler: function () {
                    peerManager.newPeerWin.show();
                }
            },
            {
                xtype: 'button',
                iconCls: 'icon-remove',
                text: 'Peer törlése',
                handler: function () {
                    var gridrecord = peerManager.grid.getSelectionModel().getSelected();
                    Ext.MessageBox.confirm("Megerősítés", "Biztosan törölni kívánja a kiválasztott eszközt?", function (btn) {
                        if (btn == 'yes') {
                            Ext.Ajax.request({
                                url: 'ajax/buddies/del/' + gridrecord['id'],
                                method: 'POST',
                                success: function (frm, act) {
                                    peerManager.ds.reload();
                                }
                            });
                        }
                    });

                }
            },
            {
                xtype: 'button',
                iconCls: 'icon-refresh',
                text: 'Frissítés',
                handler: function () {
                    ds.reload();
                }
            }
        ]
    });

    this.grid.on('afteredit', function (grid_event) {
        $.post("ajax/updatePeers.php", {
            id: peerManager.ds.data.items[grid_event.row].id,
            field: grid_event.field,
            value: grid_event.value
        }, function () {
            peerManager.ds.reload();
        });
    });


    // INIT
    this.ds.load();
    this.contextStore.load();

    this.getGrid = function () {
        return peerManager.grid;
    }
};