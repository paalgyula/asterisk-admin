Ext.onReady(function () {

    var jsonContextReader = new Ext.data.JsonReader({
        root: 'results',
        id: 'id'
    }, [
        {name: 'id', type: 'int', mapping: 'id'},
        {name: 'context', type: 'string', mapping: 'context'},
        {name: 'exten', type: 'string', mapping: 'exten'},
        {name: 'priority', type: 'int', mapping: 'priority'},
        {name: 'app', type: 'string', mapping: 'app'},
        {name: 'appData', type: 'string', mapping: 'appData'}
    ]);

    contextDataStore = new Ext.data.Store({
        reader: jsonContextReader,
        sortInfo: { field: 'context', direction: 'ASC' },
        proxy: new Ext.data.ScriptTagProxy({
            url: 'ajax/contextList.php'
        })
    });

    contextDataStore.load();

    contextColModel = new Ext.grid.ColumnModel({
        columns: [
            {
                header: "ID",
                dataIndex: 'id',
                width: 40,
                hidden: true
            },
            {
                header: "Context neve",
                dataIndex: 'context',
                width: 150,
                editor: new Ext.form.TextField({
                    allowBlank: false,
                    maxLength: 20,
                    width: 150
                })
            },
            {
                header: "Mellék",
                dataIndex: 'exten'
            },
            {
                header: "Prioritás",
                dataIndex: 'priority'
            },
            {
                header: "Program",
                dataIndex: 'app'
            },
            {
                header: "Program paraméterek",
                dataIndex: 'appData',
                width: 200
            }
        ] });

    contextGrid = new Ext.grid.EditorGridPanel({
        colModel: contextColModel,
        store: contextDataStore,
        remoteSort: true,
        selModel: new Ext.grid.RowSelectionModel,
        bbar: new Ext.PagingToolbar({
            pageSize: 25,
            store: contextDataStore,
            displayInfo: true,
            displayMsg: 'Displaying contexts {0} - {1} of {2}'
        }),
        tbar: [
            {
                xtype: 'button',
                text: 'Új Szabály',
                iconCls: 'icon-add',
                handler: function () {
                    contextNewWin.show();
                }
            },
            {
                xtype: 'button',
                iconCls: 'icon-remove',
                text: 'Szabály törlése',
                handler: function () {
                    var gridrecord = contextGrid.getSelectionModel().getSelected();
                    Ext.MessageBox.confirm("Megerősítés", "Biztosan törölni kívánja a kiválasztott szabályt?", function (btn) {
                        if (btn == 'yes') {
                            $.post("ajax/contextDel.php", { id: gridrecord['id'] }, function () {
                                contextDataStore.reload();
                            });
                        }
                    });

                }
            },
            {
                xtype: 'button',
                iconCls: 'icon-refresh',
                text: 'Frissítés',
                handler: function () {
                    contextDataStore.reload();
                }
            }
        ]
    });
});
