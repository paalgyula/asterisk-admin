Ext.onReady(function () {

    Ext.QuickTips.init();

    programStorage = new Ext.data.ArrayStore({
        fields: [
            'value'
        ],
        data: [
            [ "Dial" ],
            [ "Set" ],
            [ "Voicemail" ],
            [ "NoOp" ],
            [ "GotoIf" ],
            [ "Answer" ],
            [ "Wait" ],
            [ "MeetMe" ],
            [ "Playback" ],
            [ "SayDigits" ],
            [ "Echo" ],
            [ "Record" ],
            [ "Authenticate" ],
            [ "MusicOnHold" ],
            [ "AGI" ],
            [ "Background" ],
            [ "WaitExten" ],
            [ "Goto" ],
            [ "Macro" ],
            [ "Hangup" ]
        ]
    });

    contextNewForm = new Ext.FormPanel({
        labelWidth: 130,
        title: 'Új Szabály',
        border: true,
        frame: true,
        defaultType: 'textfield',
        buttonAlign: 'center',
        monitorValid: true,
        defaults: {
            allowBlank: false,
            blankText: 'A mező kitöltése kötelező!',
            width: 200
        },
        items: [
            {
                fieldLabel: 'Context név',
                id: 'context'
            },
            {
                fieldLabel: 'Mellék',
                id: 'mellek'
            },
            {
                fieldLabel: 'Priority',
                id: 'prioritas'
            },
            {
                fieldLabel: 'Program',
                id: 'app',
                xtype: 'combo',
                minChars: 1,
                forceSelection: false,
                typeAhead: true,
                mode: 'local',
                triggerAction: 'all',
                store: programStorage,
                valueField: 'value',
                displayField: 'value'
            },
            {
                allowBlank: true,
                fieldLabel: 'Paraméterek',
                id: 'appData',
                xtype: 'textarea'
            }
        ],
        buttons: [
            {
                text: 'Felvétel',
                iconCls: 'icon-add',
                formBind: true,
                handler: function () {
                    contextNewForm.getForm().submit({
                        url: "ajax/contextAdd.php",
                        waitMsg: 'Szabály hozzáadása...',
                        failure: function (frm, act) {
                            Ext.MessageBox.alert('Hiba', 'Ismeretlen hiba miatt a szabály nem került felvételre');
                        },
                        success: function (frm, act) {
                            contextDataStore.reload();
                            contextNewWin.hide();
                        }
                    })
                }
            },
            {
                text: 'Mégsem',
                handler: function () {
                    contextNewForm.getForm().reset();
                    contextNewWin.hide();
                }
            },
            {
                text: 'Reset',
                handler: function () {
                    contextNewForm.getForm().reset();
                }
            }
        ]
    });

    contextNewWin = new Ext.Window({
        width: 370,
        modal: true,
        resizable: false,
        draggable: false,
        closable: false,
        border: false,
        items: [
            contextNewForm
        ]
    });

    //contextNewWin.show();
});
