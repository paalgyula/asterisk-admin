package com.gwsystems.asteriskadmin.entity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Entity
@javax.persistence.Table(name = "queues", schema = "", catalog = "pbx")
public class Queue {
    private String name;

    @Id
    @javax.persistence.Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String musiconhold;

    @Basic
    @javax.persistence.Column(name = "musiconhold")
    public String getMusiconhold() {
        return musiconhold;
    }

    public void setMusiconhold(String musiconhold) {
        this.musiconhold = musiconhold;
    }

    private String announce;

    @Basic
    @javax.persistence.Column(name = "announce")
    public String getAnnounce() {
        return announce;
    }

    public void setAnnounce(String announce) {
        this.announce = announce;
    }

    private String context;

    @Basic
    @javax.persistence.Column(name = "context")
    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    private Integer timeout;

    @Basic
    @javax.persistence.Column(name = "timeout")
    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    private String monitorType;

    @Basic
    @javax.persistence.Column(name = "monitor_type")
    public String getMonitorType() {
        return monitorType;
    }

    public void setMonitorType(String monitorType) {
        this.monitorType = monitorType;
    }

    private String monitorFormat;

    @Basic
    @javax.persistence.Column(name = "monitor_format")
    public String getMonitorFormat() {
        return monitorFormat;
    }

    public void setMonitorFormat(String monitorFormat) {
        this.monitorFormat = monitorFormat;
    }

    private String queueYouarenext;

    @Basic
    @javax.persistence.Column(name = "queue_youarenext")
    public String getQueueYouarenext() {
        return queueYouarenext;
    }

    public void setQueueYouarenext(String queueYouarenext) {
        this.queueYouarenext = queueYouarenext;
    }

    private String queueThereare;

    @Basic
    @javax.persistence.Column(name = "queue_thereare")
    public String getQueueThereare() {
        return queueThereare;
    }

    public void setQueueThereare(String queueThereare) {
        this.queueThereare = queueThereare;
    }

    private String queueCallswaiting;

    @Basic
    @javax.persistence.Column(name = "queue_callswaiting")
    public String getQueueCallswaiting() {
        return queueCallswaiting;
    }

    public void setQueueCallswaiting(String queueCallswaiting) {
        this.queueCallswaiting = queueCallswaiting;
    }

    private String queueHoldtime;

    @Basic
    @javax.persistence.Column(name = "queue_holdtime")
    public String getQueueHoldtime() {
        return queueHoldtime;
    }

    public void setQueueHoldtime(String queueHoldtime) {
        this.queueHoldtime = queueHoldtime;
    }

    private String queueMinutes;

    @Basic
    @javax.persistence.Column(name = "queue_minutes")
    public String getQueueMinutes() {
        return queueMinutes;
    }

    public void setQueueMinutes(String queueMinutes) {
        this.queueMinutes = queueMinutes;
    }

    private String queueSeconds;

    @Basic
    @javax.persistence.Column(name = "queue_seconds")
    public String getQueueSeconds() {
        return queueSeconds;
    }

    public void setQueueSeconds(String queueSeconds) {
        this.queueSeconds = queueSeconds;
    }

    private String queueLessthan;

    @Basic
    @javax.persistence.Column(name = "queue_lessthan")
    public String getQueueLessthan() {
        return queueLessthan;
    }

    public void setQueueLessthan(String queueLessthan) {
        this.queueLessthan = queueLessthan;
    }

    private String queueThankyou;

    @Basic
    @javax.persistence.Column(name = "queue_thankyou")
    public String getQueueThankyou() {
        return queueThankyou;
    }

    public void setQueueThankyou(String queueThankyou) {
        this.queueThankyou = queueThankyou;
    }

    private String queueReporthold;

    @Basic
    @javax.persistence.Column(name = "queue_reporthold")
    public String getQueueReporthold() {
        return queueReporthold;
    }

    public void setQueueReporthold(String queueReporthold) {
        this.queueReporthold = queueReporthold;
    }

    private Integer announceFrequency;

    @Basic
    @javax.persistence.Column(name = "announce_frequency")
    public Integer getAnnounceFrequency() {
        return announceFrequency;
    }

    public void setAnnounceFrequency(Integer announceFrequency) {
        this.announceFrequency = announceFrequency;
    }

    private Integer announceRoundSeconds;

    @Basic
    @javax.persistence.Column(name = "announce_round_seconds")
    public Integer getAnnounceRoundSeconds() {
        return announceRoundSeconds;
    }

    public void setAnnounceRoundSeconds(Integer announceRoundSeconds) {
        this.announceRoundSeconds = announceRoundSeconds;
    }

    private String announceHoldtime;

    @Basic
    @javax.persistence.Column(name = "announce_holdtime")
    public String getAnnounceHoldtime() {
        return announceHoldtime;
    }

    public void setAnnounceHoldtime(String announceHoldtime) {
        this.announceHoldtime = announceHoldtime;
    }

    private Integer retry;

    @Basic
    @javax.persistence.Column(name = "retry")
    public Integer getRetry() {
        return retry;
    }

    public void setRetry(Integer retry) {
        this.retry = retry;
    }

    private Integer wrapuptime;

    @Basic
    @javax.persistence.Column(name = "wrapuptime")
    public Integer getWrapuptime() {
        return wrapuptime;
    }

    public void setWrapuptime(Integer wrapuptime) {
        this.wrapuptime = wrapuptime;
    }

    private Integer maxlen;

    @Basic
    @javax.persistence.Column(name = "maxlen")
    public Integer getMaxlen() {
        return maxlen;
    }

    public void setMaxlen(Integer maxlen) {
        this.maxlen = maxlen;
    }

    private Integer servicelevel;

    @Basic
    @javax.persistence.Column(name = "servicelevel")
    public Integer getServicelevel() {
        return servicelevel;
    }

    public void setServicelevel(Integer servicelevel) {
        this.servicelevel = servicelevel;
    }

    private String strategy;

    @Basic
    @javax.persistence.Column(name = "strategy")
    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    private String joinempty;

    @Basic
    @javax.persistence.Column(name = "joinempty")
    public String getJoinempty() {
        return joinempty;
    }

    public void setJoinempty(String joinempty) {
        this.joinempty = joinempty;
    }

    private String leavewhenempty;

    @Basic
    @javax.persistence.Column(name = "leavewhenempty")
    public String getLeavewhenempty() {
        return leavewhenempty;
    }

    public void setLeavewhenempty(String leavewhenempty) {
        this.leavewhenempty = leavewhenempty;
    }

    private String eventmemberstatus;

    @Basic
    @javax.persistence.Column(name = "eventmemberstatus")
    public String getEventmemberstatus() {
        return eventmemberstatus;
    }

    public void setEventmemberstatus(String eventmemberstatus) {
        this.eventmemberstatus = eventmemberstatus;
    }

    private String eventwhencalled;

    @Basic
    @javax.persistence.Column(name = "eventwhencalled")
    public String getEventwhencalled() {
        return eventwhencalled;
    }

    public void setEventwhencalled(String eventwhencalled) {
        this.eventwhencalled = eventwhencalled;
    }

    private Boolean reportholdtime;

    @Basic
    @javax.persistence.Column(name = "reportholdtime")
    public Boolean getReportholdtime() {
        return reportholdtime;
    }

    public void setReportholdtime(Boolean reportholdtime) {
        this.reportholdtime = reportholdtime;
    }

    private Integer memberdelay;

    @Basic
    @javax.persistence.Column(name = "memberdelay")
    public Integer getMemberdelay() {
        return memberdelay;
    }

    public void setMemberdelay(Integer memberdelay) {
        this.memberdelay = memberdelay;
    }

    private Integer weight;

    @Basic
    @javax.persistence.Column(name = "weight")
    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    private Boolean timeoutrestart;

    @Basic
    @javax.persistence.Column(name = "timeoutrestart")
    public Boolean getTimeoutrestart() {
        return timeoutrestart;
    }

    public void setTimeoutrestart(Boolean timeoutrestart) {
        this.timeoutrestart = timeoutrestart;
    }

    private String periodicAnnounce;

    @Basic
    @javax.persistence.Column(name = "periodic_announce")
    public String getPeriodicAnnounce() {
        return periodicAnnounce;
    }

    public void setPeriodicAnnounce(String periodicAnnounce) {
        this.periodicAnnounce = periodicAnnounce;
    }

    private Integer periodicAnnounceFrequency;

    @Basic
    @javax.persistence.Column(name = "periodic_announce_frequency")
    public Integer getPeriodicAnnounceFrequency() {
        return periodicAnnounceFrequency;
    }

    public void setPeriodicAnnounceFrequency(Integer periodicAnnounceFrequency) {
        this.periodicAnnounceFrequency = periodicAnnounceFrequency;
    }

    private Boolean ringinuse;

    @Basic
    @javax.persistence.Column(name = "ringinuse")
    public Boolean getRinginuse() {
        return ringinuse;
    }

    public void setRinginuse(Boolean ringinuse) {
        this.ringinuse = ringinuse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Queue queue = (Queue) o;

        if (announce != null ? !announce.equals(queue.announce) : queue.announce != null) return false;
        if (announceFrequency != null ? !announceFrequency.equals(queue.announceFrequency) : queue.announceFrequency != null)
            return false;
        if (announceHoldtime != null ? !announceHoldtime.equals(queue.announceHoldtime) : queue.announceHoldtime != null)
            return false;
        if (announceRoundSeconds != null ? !announceRoundSeconds.equals(queue.announceRoundSeconds) : queue.announceRoundSeconds != null)
            return false;
        if (context != null ? !context.equals(queue.context) : queue.context != null) return false;
        if (eventmemberstatus != null ? !eventmemberstatus.equals(queue.eventmemberstatus) : queue.eventmemberstatus != null)
            return false;
        if (eventwhencalled != null ? !eventwhencalled.equals(queue.eventwhencalled) : queue.eventwhencalled != null)
            return false;
        if (joinempty != null ? !joinempty.equals(queue.joinempty) : queue.joinempty != null) return false;
        if (leavewhenempty != null ? !leavewhenempty.equals(queue.leavewhenempty) : queue.leavewhenempty != null)
            return false;
        if (maxlen != null ? !maxlen.equals(queue.maxlen) : queue.maxlen != null) return false;
        if (memberdelay != null ? !memberdelay.equals(queue.memberdelay) : queue.memberdelay != null) return false;
        if (monitorFormat != null ? !monitorFormat.equals(queue.monitorFormat) : queue.monitorFormat != null)
            return false;
        if (monitorType != null ? !monitorType.equals(queue.monitorType) : queue.monitorType != null) return false;
        if (musiconhold != null ? !musiconhold.equals(queue.musiconhold) : queue.musiconhold != null) return false;
        if (name != null ? !name.equals(queue.name) : queue.name != null) return false;
        if (periodicAnnounce != null ? !periodicAnnounce.equals(queue.periodicAnnounce) : queue.periodicAnnounce != null)
            return false;
        if (periodicAnnounceFrequency != null ? !periodicAnnounceFrequency.equals(queue.periodicAnnounceFrequency) : queue.periodicAnnounceFrequency != null)
            return false;
        if (queueCallswaiting != null ? !queueCallswaiting.equals(queue.queueCallswaiting) : queue.queueCallswaiting != null)
            return false;
        if (queueHoldtime != null ? !queueHoldtime.equals(queue.queueHoldtime) : queue.queueHoldtime != null)
            return false;
        if (queueLessthan != null ? !queueLessthan.equals(queue.queueLessthan) : queue.queueLessthan != null)
            return false;
        if (queueMinutes != null ? !queueMinutes.equals(queue.queueMinutes) : queue.queueMinutes != null) return false;
        if (queueReporthold != null ? !queueReporthold.equals(queue.queueReporthold) : queue.queueReporthold != null)
            return false;
        if (queueSeconds != null ? !queueSeconds.equals(queue.queueSeconds) : queue.queueSeconds != null) return false;
        if (queueThankyou != null ? !queueThankyou.equals(queue.queueThankyou) : queue.queueThankyou != null)
            return false;
        if (queueThereare != null ? !queueThereare.equals(queue.queueThereare) : queue.queueThereare != null)
            return false;
        if (queueYouarenext != null ? !queueYouarenext.equals(queue.queueYouarenext) : queue.queueYouarenext != null)
            return false;
        if (reportholdtime != null ? !reportholdtime.equals(queue.reportholdtime) : queue.reportholdtime != null)
            return false;
        if (retry != null ? !retry.equals(queue.retry) : queue.retry != null) return false;
        if (ringinuse != null ? !ringinuse.equals(queue.ringinuse) : queue.ringinuse != null) return false;
        if (servicelevel != null ? !servicelevel.equals(queue.servicelevel) : queue.servicelevel != null) return false;
        if (strategy != null ? !strategy.equals(queue.strategy) : queue.strategy != null) return false;
        if (timeout != null ? !timeout.equals(queue.timeout) : queue.timeout != null) return false;
        if (timeoutrestart != null ? !timeoutrestart.equals(queue.timeoutrestart) : queue.timeoutrestart != null)
            return false;
        if (weight != null ? !weight.equals(queue.weight) : queue.weight != null) return false;
        if (wrapuptime != null ? !wrapuptime.equals(queue.wrapuptime) : queue.wrapuptime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (musiconhold != null ? musiconhold.hashCode() : 0);
        result = 31 * result + (announce != null ? announce.hashCode() : 0);
        result = 31 * result + (context != null ? context.hashCode() : 0);
        result = 31 * result + (timeout != null ? timeout.hashCode() : 0);
        result = 31 * result + (monitorType != null ? monitorType.hashCode() : 0);
        result = 31 * result + (monitorFormat != null ? monitorFormat.hashCode() : 0);
        result = 31 * result + (queueYouarenext != null ? queueYouarenext.hashCode() : 0);
        result = 31 * result + (queueThereare != null ? queueThereare.hashCode() : 0);
        result = 31 * result + (queueCallswaiting != null ? queueCallswaiting.hashCode() : 0);
        result = 31 * result + (queueHoldtime != null ? queueHoldtime.hashCode() : 0);
        result = 31 * result + (queueMinutes != null ? queueMinutes.hashCode() : 0);
        result = 31 * result + (queueSeconds != null ? queueSeconds.hashCode() : 0);
        result = 31 * result + (queueLessthan != null ? queueLessthan.hashCode() : 0);
        result = 31 * result + (queueThankyou != null ? queueThankyou.hashCode() : 0);
        result = 31 * result + (queueReporthold != null ? queueReporthold.hashCode() : 0);
        result = 31 * result + (announceFrequency != null ? announceFrequency.hashCode() : 0);
        result = 31 * result + (announceRoundSeconds != null ? announceRoundSeconds.hashCode() : 0);
        result = 31 * result + (announceHoldtime != null ? announceHoldtime.hashCode() : 0);
        result = 31 * result + (retry != null ? retry.hashCode() : 0);
        result = 31 * result + (wrapuptime != null ? wrapuptime.hashCode() : 0);
        result = 31 * result + (maxlen != null ? maxlen.hashCode() : 0);
        result = 31 * result + (servicelevel != null ? servicelevel.hashCode() : 0);
        result = 31 * result + (strategy != null ? strategy.hashCode() : 0);
        result = 31 * result + (joinempty != null ? joinempty.hashCode() : 0);
        result = 31 * result + (leavewhenempty != null ? leavewhenempty.hashCode() : 0);
        result = 31 * result + (eventmemberstatus != null ? eventmemberstatus.hashCode() : 0);
        result = 31 * result + (eventwhencalled != null ? eventwhencalled.hashCode() : 0);
        result = 31 * result + (reportholdtime != null ? reportholdtime.hashCode() : 0);
        result = 31 * result + (memberdelay != null ? memberdelay.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        result = 31 * result + (timeoutrestart != null ? timeoutrestart.hashCode() : 0);
        result = 31 * result + (periodicAnnounce != null ? periodicAnnounce.hashCode() : 0);
        result = 31 * result + (periodicAnnounceFrequency != null ? periodicAnnounceFrequency.hashCode() : 0);
        result = 31 * result + (ringinuse != null ? ringinuse.hashCode() : 0);
        return result;
    }
}
