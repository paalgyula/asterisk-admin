package com.gwsystems.asteriskadmin.entity;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Entity
@javax.persistence.Table(name = "queue_members", schema = "", catalog = "pbx")
public class QueueMembers {
    private int uniqueid;
    private String membername;
    private String queueName;
    private String iface;
    private Integer penalty;
    private Boolean paused;

    @Id
    @javax.persistence.Column(name = "uniqueid")
    public int getUniqueid() {
        return uniqueid;
    }

    public void setUniqueid(int uniqueid) {
        this.uniqueid = uniqueid;
    }

    @Basic
    @javax.persistence.Column(name = "membername")
    public String getMembername() {
        return membername;
    }

    public void setMembername(String membername) {
        this.membername = membername;
    }

    @Basic
    @javax.persistence.Column(name = "queue_name")
    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    @Basic
    @javax.persistence.Column(name = "interface")
    public String getIface() {
        return iface;
    }

    public void setIface(String iface) {
        this.iface = iface;
    }

    @Basic
    @javax.persistence.Column(name = "penalty")
    public Integer getPenalty() {
        return penalty;
    }

    public void setPenalty(Integer penalty) {
        this.penalty = penalty;
    }

    @Basic
    @javax.persistence.Column(name = "paused")
    public Boolean getPaused() {
        return paused;
    }

    public void setPaused(Boolean paused) {
        this.paused = paused;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QueueMembers that = (QueueMembers) o;

        if (uniqueid != that.uniqueid) return false;
        if (iface != null ? !iface.equals(that.iface) : that.iface != null) return false;
        if (membername != null ? !membername.equals(that.membername) : that.membername != null) return false;
        if (paused != null ? !paused.equals(that.paused) : that.paused != null) return false;
        if (penalty != null ? !penalty.equals(that.penalty) : that.penalty != null) return false;
        if (queueName != null ? !queueName.equals(that.queueName) : that.queueName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uniqueid;
        result = 31 * result + (membername != null ? membername.hashCode() : 0);
        result = 31 * result + (queueName != null ? queueName.hashCode() : 0);
        result = 31 * result + (iface != null ? iface.hashCode() : 0);
        result = 31 * result + (penalty != null ? penalty.hashCode() : 0);
        result = 31 * result + (paused != null ? paused.hashCode() : 0);
        return result;
    }
}
