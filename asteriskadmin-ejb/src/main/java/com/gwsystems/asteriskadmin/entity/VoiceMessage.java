package com.gwsystems.asteriskadmin.entity;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Entity
@Table(name = "voicemessages", schema = "", catalog = "pbx")
public class VoiceMessage {
    private int di;
    private int msgnum;
    private String dir;
    private String context;
    private String macrocontext;
    private String callerid;
    private String origtime;
    private String duration;
    private String mailbox;
    private String mailboxcontext;
    private byte[] recording;

    @Id
    @Column(name = "di")
    public int getDi() {
        return di;
    }

    public void setDi(int di) {
        this.di = di;
    }

    @Basic
    @Column(name = "msgnum")
    public int getMsgnum() {
        return msgnum;
    }

    public void setMsgnum(int msgnum) {
        this.msgnum = msgnum;
    }

    @Basic
    @Column(name = "dir")
    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    @Basic
    @Column(name = "context")
    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Basic
    @Column(name = "macrocontext")
    public String getMacrocontext() {
        return macrocontext;
    }

    public void setMacrocontext(String macrocontext) {
        this.macrocontext = macrocontext;
    }

    @Basic
    @Column(name = "callerid")
    public String getCallerid() {
        return callerid;
    }

    public void setCallerid(String callerid) {
        this.callerid = callerid;
    }

    @Basic
    @Column(name = "origtime")
    public String getOrigtime() {
        return origtime;
    }

    public void setOrigtime(String origtime) {
        this.origtime = origtime;
    }

    @Basic
    @Column(name = "duration")
    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "mailbox")
    public String getMailbox() {
        return mailbox;
    }

    public void setMailbox(String mailbox) {
        this.mailbox = mailbox;
    }

    @Basic
    @Column(name = "mailboxcontext")
    public String getMailboxcontext() {
        return mailboxcontext;
    }

    public void setMailboxcontext(String mailboxcontext) {
        this.mailboxcontext = mailboxcontext;
    }

    @Basic
    @Column(name = "recording")
    public byte[] getRecording() {
        return recording;
    }

    public void setRecording(byte[] recording) {
        this.recording = recording;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VoiceMessage that = (VoiceMessage) o;

        if (di != that.di) return false;
        if (msgnum != that.msgnum) return false;
        if (callerid != null ? !callerid.equals(that.callerid) : that.callerid != null) return false;
        if (context != null ? !context.equals(that.context) : that.context != null) return false;
        if (dir != null ? !dir.equals(that.dir) : that.dir != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (macrocontext != null ? !macrocontext.equals(that.macrocontext) : that.macrocontext != null) return false;
        if (mailbox != null ? !mailbox.equals(that.mailbox) : that.mailbox != null) return false;
        if (mailboxcontext != null ? !mailboxcontext.equals(that.mailboxcontext) : that.mailboxcontext != null)
            return false;
        if (origtime != null ? !origtime.equals(that.origtime) : that.origtime != null) return false;
        if (!Arrays.equals(recording, that.recording)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = di;
        result = 31 * result + msgnum;
        result = 31 * result + (dir != null ? dir.hashCode() : 0);
        result = 31 * result + (context != null ? context.hashCode() : 0);
        result = 31 * result + (macrocontext != null ? macrocontext.hashCode() : 0);
        result = 31 * result + (callerid != null ? callerid.hashCode() : 0);
        result = 31 * result + (origtime != null ? origtime.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (mailbox != null ? mailbox.hashCode() : 0);
        result = 31 * result + (mailboxcontext != null ? mailboxcontext.hashCode() : 0);
        result = 31 * result + (recording != null ? Arrays.hashCode(recording) : 0);
        return result;
    }
}
