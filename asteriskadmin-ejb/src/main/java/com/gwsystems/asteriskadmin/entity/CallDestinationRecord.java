package com.gwsystems.asteriskadmin.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Entity
@Table(name = "cdr")
public class CallDestinationRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Timestamp calldate;
    private String clid;
    private String src;
    private String dst;
    private String dcontext;
    private String channel;
    private String dstchannel;
    private String lastapp;
    private String lastdata;
    private int duration;
    private int billsec;
    private String disposition;
    private int amaflags;
    private String accountcode;
    private String uniqueid;
    private String userfield;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "calldate")
    public Timestamp getCalldate() {
        return calldate;
    }

    public void setCalldate(Timestamp calldate) {
        this.calldate = calldate;
    }

    @Basic
    @Column(name = "clid")
    public String getClid() {
        return clid;
    }

    public void setClid(String clid) {
        this.clid = clid;
    }

    @Basic
    @Column(name = "src")
    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Basic
    @Column(name = "dst")
    public String getDst() {
        return dst;
    }

    public void setDst(String dst) {
        this.dst = dst;
    }

    @Basic
    @Column(name = "dcontext")
    public String getDcontext() {
        return dcontext;
    }

    public void setDcontext(String dcontext) {
        this.dcontext = dcontext;
    }

    @Basic
    @Column(name = "channel")
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Basic
    @Column(name = "dstchannel")
    public String getDstchannel() {
        return dstchannel;
    }

    public void setDstchannel(String dstchannel) {
        this.dstchannel = dstchannel;
    }

    @Basic
    @Column(name = "lastapp")
    public String getLastapp() {
        return lastapp;
    }

    public void setLastapp(String lastapp) {
        this.lastapp = lastapp;
    }

    @Basic
    @Column(name = "lastdata")
    public String getLastdata() {
        return lastdata;
    }

    public void setLastdata(String lastdata) {
        this.lastdata = lastdata;
    }

    @Basic
    @Column(name = "duration")
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "billsec")
    public int getBillsec() {
        return billsec;
    }

    public void setBillsec(int billsec) {
        this.billsec = billsec;
    }

    @Basic
    @Column(name = "disposition")
    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    @Basic
    @Column(name = "amaflags")
    public int getAmaflags() {
        return amaflags;
    }

    public void setAmaflags(int amaflags) {
        this.amaflags = amaflags;
    }

    @Basic
    @Column(name = "accountcode")
    public String getAccountcode() {
        return accountcode;
    }

    public void setAccountcode(String accountcode) {
        this.accountcode = accountcode;
    }

    @Basic
    @Column(name = "uniqueid")
    public String getUniqueid() {
        return uniqueid;
    }

    public void setUniqueid(String uniqueid) {
        this.uniqueid = uniqueid;
    }

    @Basic
    @Column(name = "userfield")
    public String getUserfield() {
        return userfield;
    }

    public void setUserfield(String userfield) {
        this.userfield = userfield;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CallDestinationRecord that = (CallDestinationRecord) o;

        if (amaflags != that.amaflags) return false;
        if (billsec != that.billsec) return false;
        if (duration != that.duration) return false;
        if (accountcode != null ? !accountcode.equals(that.accountcode) : that.accountcode != null) return false;
        if (calldate != null ? !calldate.equals(that.calldate) : that.calldate != null) return false;
        if (channel != null ? !channel.equals(that.channel) : that.channel != null) return false;
        if (clid != null ? !clid.equals(that.clid) : that.clid != null) return false;
        if (dcontext != null ? !dcontext.equals(that.dcontext) : that.dcontext != null) return false;
        if (disposition != null ? !disposition.equals(that.disposition) : that.disposition != null) return false;
        if (dst != null ? !dst.equals(that.dst) : that.dst != null) return false;
        if (dstchannel != null ? !dstchannel.equals(that.dstchannel) : that.dstchannel != null) return false;
        if (lastapp != null ? !lastapp.equals(that.lastapp) : that.lastapp != null) return false;
        if (lastdata != null ? !lastdata.equals(that.lastdata) : that.lastdata != null) return false;
        if (src != null ? !src.equals(that.src) : that.src != null) return false;
        if (uniqueid != null ? !uniqueid.equals(that.uniqueid) : that.uniqueid != null) return false;
        if (userfield != null ? !userfield.equals(that.userfield) : that.userfield != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = calldate != null ? calldate.hashCode() : 0;
        result = 31 * result + (clid != null ? clid.hashCode() : 0);
        result = 31 * result + (src != null ? src.hashCode() : 0);
        result = 31 * result + (dst != null ? dst.hashCode() : 0);
        result = 31 * result + (dcontext != null ? dcontext.hashCode() : 0);
        result = 31 * result + (channel != null ? channel.hashCode() : 0);
        result = 31 * result + (dstchannel != null ? dstchannel.hashCode() : 0);
        result = 31 * result + (lastapp != null ? lastapp.hashCode() : 0);
        result = 31 * result + (lastdata != null ? lastdata.hashCode() : 0);
        result = 31 * result + duration;
        result = 31 * result + billsec;
        result = 31 * result + (disposition != null ? disposition.hashCode() : 0);
        result = 31 * result + amaflags;
        result = 31 * result + (accountcode != null ? accountcode.hashCode() : 0);
        result = 31 * result + (uniqueid != null ? uniqueid.hashCode() : 0);
        result = 31 * result + (userfield != null ? userfield.hashCode() : 0);
        return result;
    }
}
