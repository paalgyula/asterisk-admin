package com.gwsystems.asteriskadmin.entity;

import javax.persistence.*;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Entity
@Table(name = "extensions", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"context", "exten", "priority"})
})
public class Extension {
    private int id;
    private String context;
    private String exten;
    private byte priority;
    private String app;
    private String appdata;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "context")
    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    @Basic
    @Column(name = "exten")
    public String getExten() {
        return exten;
    }

    public void setExten(String exten) {
        this.exten = exten;
    }

    @Basic
    @Column(name = "priority")
    public byte getPriority() {
        return priority;
    }

    public void setPriority(byte priority) {
        this.priority = priority;
    }

    @Basic
    @Column(name = "app")
    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    @Basic
    @Column(name = "appdata")
    public String getAppdata() {
        return appdata;
    }

    public void setAppdata(String appdata) {
        this.appdata = appdata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Extension extension = (Extension) o;

        if (id != extension.id) return false;
        if (priority != extension.priority) return false;
        if (app != null ? !app.equals(extension.app) : extension.app != null) return false;
        if (appdata != null ? !appdata.equals(extension.appdata) : extension.appdata != null) return false;
        if (context != null ? !context.equals(extension.context) : extension.context != null) return false;
        if (exten != null ? !exten.equals(extension.exten) : extension.exten != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (context != null ? context.hashCode() : 0);
        result = 31 * result + (exten != null ? exten.hashCode() : 0);
        result = 31 * result + (int) priority;
        result = 31 * result + (app != null ? app.hashCode() : 0);
        result = 31 * result + (appdata != null ? appdata.hashCode() : 0);
        return result;
    }
}
