package com.gwsystems.asteriskadmin.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Entity
@Table(name = "sip_buddies")
public class SipBuddy {
    private int id;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String name;

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String accountcode;

    @Basic
    @Column(name = "accountcode")
    public String getAccountcode() {
        return accountcode;
    }

    public void setAccountcode(String accountcode) {
        this.accountcode = accountcode;
    }

    private String amaflags;

    @Basic
    @Column(name = "amaflags")
    public String getAmaflags() {
        return amaflags;
    }

    public void setAmaflags(String amaflags) {
        this.amaflags = amaflags;
    }

    private String callgroup;

    @Basic
    @Column(name = "callgroup")
    public String getCallgroup() {
        return callgroup;
    }

    public void setCallgroup(String callgroup) {
        this.callgroup = callgroup;
    }

    private String callerid;

    @Basic
    @Column(name = "callerid")
    public String getCallerid() {
        return callerid;
    }

    public void setCallerid(String callerid) {
        this.callerid = callerid;
    }

    private String canreinvite;

    @Basic
    @Column(name = "canreinvite")
    public String getCanreinvite() {
        return canreinvite;
    }

    public void setCanreinvite(String canreinvite) {
        this.canreinvite = canreinvite;
    }

    private String context;

    @Basic
    @Column(name = "context")
    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    private String defaultip;

    @Basic
    @Column(name = "defaultip")
    public String getDefaultip() {
        return defaultip;
    }

    public void setDefaultip(String defaultip) {
        this.defaultip = defaultip;
    }

    private String dtmfmode;

    @Basic
    @Column(name = "dtmfmode")
    public String getDtmfmode() {
        return dtmfmode;
    }

    public void setDtmfmode(String dtmfmode) {
        this.dtmfmode = dtmfmode;
    }

    private String fromuser;

    @Basic
    @Column(name = "fromuser")
    public String getFromuser() {
        return fromuser;
    }

    public void setFromuser(String fromuser) {
        this.fromuser = fromuser;
    }

    private String fromdomain;

    @Basic
    @Column(name = "fromdomain")
    public String getFromdomain() {
        return fromdomain;
    }

    public void setFromdomain(String fromdomain) {
        this.fromdomain = fromdomain;
    }

    private String fullcontact;

    @Basic
    @Column(name = "fullcontact")
    public String getFullcontact() {
        return fullcontact;
    }

    public void setFullcontact(String fullcontact) {
        this.fullcontact = fullcontact;
    }

    private String host;

    @Basic
    @Column(name = "host")
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    private String insecure;

    @Basic
    @Column(name = "insecure")
    public String getInsecure() {
        return insecure;
    }

    public void setInsecure(String insecure) {
        this.insecure = insecure;
    }

    private String language;

    @Basic
    @Column(name = "language")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    private String mailbox;

    @Basic
    @Column(name = "mailbox")
    public String getMailbox() {
        return mailbox;
    }

    public void setMailbox(String mailbox) {
        this.mailbox = mailbox;
    }

    private String md5Secret;

    @Basic
    @Column(name = "md5secret")
    public String getMd5Secret() {
        return md5Secret;
    }

    public void setMd5Secret(String md5Secret) {
        this.md5Secret = md5Secret;
    }

    private String nat;

    @Basic
    @Column(name = "nat")
    public String getNat() {
        return nat;
    }

    public void setNat(String nat) {
        this.nat = nat;
    }

    private String deny;

    @Basic
    @Column(name = "deny")
    public String getDeny() {
        return deny;
    }

    public void setDeny(String deny) {
        this.deny = deny;
    }

    private String permit;

    @Basic
    @Column(name = "permit")
    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    private String mask;

    @Basic
    @Column(name = "mask")
    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    private String pickupgroup;

    @Basic
    @Column(name = "pickupgroup")
    public String getPickupgroup() {
        return pickupgroup;
    }

    public void setPickupgroup(String pickupgroup) {
        this.pickupgroup = pickupgroup;
    }

    private String port;

    @Basic
    @Column(name = "port")
    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    private String qualify;

    @Basic
    @Column(name = "qualify")
    public String getQualify() {
        return qualify;
    }

    public void setQualify(String qualify) {
        this.qualify = qualify;
    }

    private String restrictcid;

    @Basic
    @Column(name = "restrictcid")
    public String getRestrictcid() {
        return restrictcid;
    }

    public void setRestrictcid(String restrictcid) {
        this.restrictcid = restrictcid;
    }

    private String rtptimeout;

    @Basic
    @Column(name = "rtptimeout")
    public String getRtptimeout() {
        return rtptimeout;
    }

    public void setRtptimeout(String rtptimeout) {
        this.rtptimeout = rtptimeout;
    }

    private String rtpholdtimeout;

    @Basic
    @Column(name = "rtpholdtimeout")
    public String getRtpholdtimeout() {
        return rtpholdtimeout;
    }

    public void setRtpholdtimeout(String rtpholdtimeout) {
        this.rtpholdtimeout = rtpholdtimeout;
    }

    private String secret;

    @Basic
    @Column(name = "secret")
    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    private String type;

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String username;

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String disallow;

    @Basic
    @Column(name = "disallow")
    public String getDisallow() {
        return disallow;
    }

    public void setDisallow(String disallow) {
        this.disallow = disallow;
    }

    private String allow;

    @Basic
    @Column(name = "allow")
    public String getAllow() {
        return allow;
    }

    public void setAllow(String allow) {
        this.allow = allow;
    }

    private String musiconhold;

    @Basic
    @Column(name = "musiconhold")
    public String getMusiconhold() {
        return musiconhold;
    }

    public void setMusiconhold(String musiconhold) {
        this.musiconhold = musiconhold;
    }

    private int regseconds;

    @Basic
    @Column(name = "regseconds")
    public int getRegseconds() {
        return regseconds;
    }

    public void setRegseconds(int regseconds) {
        this.regseconds = regseconds;
    }

    private String ipaddr;

    @Basic
    @Column(name = "ipaddr")
    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    private String regexten;

    @Basic
    @Column(name = "regexten")
    public String getRegexten() {
        return regexten;
    }

    public void setRegexten(String regexten) {
        this.regexten = regexten;
    }

    private String cancallforward;

    @Basic
    @Column(name = "cancallforward")
    public String getCancallforward() {
        return cancallforward;
    }

    public void setCancallforward(String cancallforward) {
        this.cancallforward = cancallforward;
    }

    private String regserver;

    @Basic
    @Column(name = "regserver")
    public String getRegserver() {
        return regserver;
    }

    public void setRegserver(String regserver) {
        this.regserver = regserver;
    }

    private String useragent;

    @Basic
    @Column(name = "useragent")
    public String getUseragent() {
        return useragent;
    }

    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    private String lastms;

    @Basic
    @Column(name = "lastms")
    public String getLastms() {
        return lastms;
    }

    public void setLastms(String lastms) {
        this.lastms = lastms;
    }

    private String defaultuser;

    @Basic
    @Column(name = "defaultuser")
    public String getDefaultuser() {
        return defaultuser;
    }

    public void setDefaultuser(String defaultuser) {
        this.defaultuser = defaultuser;
    }

    @Transient
    public String getOnline() {
        if (new Date().getTime() > regseconds)
            return "offline";
        else
            return "online";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SipBuddy sipBuddy = (SipBuddy) o;

        if (id != sipBuddy.id) return false;
        if (regseconds != sipBuddy.regseconds) return false;
        if (accountcode != null ? !accountcode.equals(sipBuddy.accountcode) : sipBuddy.accountcode != null)
            return false;
        if (allow != null ? !allow.equals(sipBuddy.allow) : sipBuddy.allow != null) return false;
        if (amaflags != null ? !amaflags.equals(sipBuddy.amaflags) : sipBuddy.amaflags != null) return false;
        if (callerid != null ? !callerid.equals(sipBuddy.callerid) : sipBuddy.callerid != null) return false;
        if (callgroup != null ? !callgroup.equals(sipBuddy.callgroup) : sipBuddy.callgroup != null) return false;
        if (cancallforward != null ? !cancallforward.equals(sipBuddy.cancallforward) : sipBuddy.cancallforward != null)
            return false;
        if (canreinvite != null ? !canreinvite.equals(sipBuddy.canreinvite) : sipBuddy.canreinvite != null)
            return false;
        if (context != null ? !context.equals(sipBuddy.context) : sipBuddy.context != null) return false;
        if (defaultip != null ? !defaultip.equals(sipBuddy.defaultip) : sipBuddy.defaultip != null) return false;
        if (defaultuser != null ? !defaultuser.equals(sipBuddy.defaultuser) : sipBuddy.defaultuser != null)
            return false;
        if (deny != null ? !deny.equals(sipBuddy.deny) : sipBuddy.deny != null) return false;
        if (disallow != null ? !disallow.equals(sipBuddy.disallow) : sipBuddy.disallow != null) return false;
        if (dtmfmode != null ? !dtmfmode.equals(sipBuddy.dtmfmode) : sipBuddy.dtmfmode != null) return false;
        if (fromdomain != null ? !fromdomain.equals(sipBuddy.fromdomain) : sipBuddy.fromdomain != null) return false;
        if (fromuser != null ? !fromuser.equals(sipBuddy.fromuser) : sipBuddy.fromuser != null) return false;
        if (fullcontact != null ? !fullcontact.equals(sipBuddy.fullcontact) : sipBuddy.fullcontact != null)
            return false;
        if (host != null ? !host.equals(sipBuddy.host) : sipBuddy.host != null) return false;
        if (insecure != null ? !insecure.equals(sipBuddy.insecure) : sipBuddy.insecure != null) return false;
        if (ipaddr != null ? !ipaddr.equals(sipBuddy.ipaddr) : sipBuddy.ipaddr != null) return false;
        if (language != null ? !language.equals(sipBuddy.language) : sipBuddy.language != null) return false;
        if (lastms != null ? !lastms.equals(sipBuddy.lastms) : sipBuddy.lastms != null) return false;
        if (mailbox != null ? !mailbox.equals(sipBuddy.mailbox) : sipBuddy.mailbox != null) return false;
        if (mask != null ? !mask.equals(sipBuddy.mask) : sipBuddy.mask != null) return false;
        if (md5Secret != null ? !md5Secret.equals(sipBuddy.md5Secret) : sipBuddy.md5Secret != null) return false;
        if (musiconhold != null ? !musiconhold.equals(sipBuddy.musiconhold) : sipBuddy.musiconhold != null)
            return false;
        if (name != null ? !name.equals(sipBuddy.name) : sipBuddy.name != null) return false;
        if (nat != null ? !nat.equals(sipBuddy.nat) : sipBuddy.nat != null) return false;
        if (permit != null ? !permit.equals(sipBuddy.permit) : sipBuddy.permit != null) return false;
        if (pickupgroup != null ? !pickupgroup.equals(sipBuddy.pickupgroup) : sipBuddy.pickupgroup != null)
            return false;
        if (port != null ? !port.equals(sipBuddy.port) : sipBuddy.port != null) return false;
        if (qualify != null ? !qualify.equals(sipBuddy.qualify) : sipBuddy.qualify != null) return false;
        if (regexten != null ? !regexten.equals(sipBuddy.regexten) : sipBuddy.regexten != null) return false;
        if (regserver != null ? !regserver.equals(sipBuddy.regserver) : sipBuddy.regserver != null) return false;
        if (restrictcid != null ? !restrictcid.equals(sipBuddy.restrictcid) : sipBuddy.restrictcid != null)
            return false;
        if (rtpholdtimeout != null ? !rtpholdtimeout.equals(sipBuddy.rtpholdtimeout) : sipBuddy.rtpholdtimeout != null)
            return false;
        if (rtptimeout != null ? !rtptimeout.equals(sipBuddy.rtptimeout) : sipBuddy.rtptimeout != null) return false;
        if (secret != null ? !secret.equals(sipBuddy.secret) : sipBuddy.secret != null) return false;
        if (type != null ? !type.equals(sipBuddy.type) : sipBuddy.type != null) return false;
        if (useragent != null ? !useragent.equals(sipBuddy.useragent) : sipBuddy.useragent != null) return false;
        if (username != null ? !username.equals(sipBuddy.username) : sipBuddy.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (accountcode != null ? accountcode.hashCode() : 0);
        result = 31 * result + (amaflags != null ? amaflags.hashCode() : 0);
        result = 31 * result + (callgroup != null ? callgroup.hashCode() : 0);
        result = 31 * result + (callerid != null ? callerid.hashCode() : 0);
        result = 31 * result + (canreinvite != null ? canreinvite.hashCode() : 0);
        result = 31 * result + (context != null ? context.hashCode() : 0);
        result = 31 * result + (defaultip != null ? defaultip.hashCode() : 0);
        result = 31 * result + (dtmfmode != null ? dtmfmode.hashCode() : 0);
        result = 31 * result + (fromuser != null ? fromuser.hashCode() : 0);
        result = 31 * result + (fromdomain != null ? fromdomain.hashCode() : 0);
        result = 31 * result + (fullcontact != null ? fullcontact.hashCode() : 0);
        result = 31 * result + (host != null ? host.hashCode() : 0);
        result = 31 * result + (insecure != null ? insecure.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (mailbox != null ? mailbox.hashCode() : 0);
        result = 31 * result + (md5Secret != null ? md5Secret.hashCode() : 0);
        result = 31 * result + (nat != null ? nat.hashCode() : 0);
        result = 31 * result + (deny != null ? deny.hashCode() : 0);
        result = 31 * result + (permit != null ? permit.hashCode() : 0);
        result = 31 * result + (mask != null ? mask.hashCode() : 0);
        result = 31 * result + (pickupgroup != null ? pickupgroup.hashCode() : 0);
        result = 31 * result + (port != null ? port.hashCode() : 0);
        result = 31 * result + (qualify != null ? qualify.hashCode() : 0);
        result = 31 * result + (restrictcid != null ? restrictcid.hashCode() : 0);
        result = 31 * result + (rtptimeout != null ? rtptimeout.hashCode() : 0);
        result = 31 * result + (rtpholdtimeout != null ? rtpholdtimeout.hashCode() : 0);
        result = 31 * result + (secret != null ? secret.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (disallow != null ? disallow.hashCode() : 0);
        result = 31 * result + (allow != null ? allow.hashCode() : 0);
        result = 31 * result + (musiconhold != null ? musiconhold.hashCode() : 0);
        result = 31 * result + regseconds;
        result = 31 * result + (ipaddr != null ? ipaddr.hashCode() : 0);
        result = 31 * result + (regexten != null ? regexten.hashCode() : 0);
        result = 31 * result + (cancallforward != null ? cancallforward.hashCode() : 0);
        result = 31 * result + (regserver != null ? regserver.hashCode() : 0);
        result = 31 * result + (useragent != null ? useragent.hashCode() : 0);
        result = 31 * result + (lastms != null ? lastms.hashCode() : 0);
        result = 31 * result + (defaultuser != null ? defaultuser.hashCode() : 0);
        return result;
    }
}
