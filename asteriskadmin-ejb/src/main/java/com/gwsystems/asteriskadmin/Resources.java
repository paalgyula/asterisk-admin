package com.gwsystems.asteriskadmin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
public class Resources {

    @PersistenceContext(unitName = "asteriskAdmin")
    private EntityManager entityManager;

    @Produces
    @AsteriskAdmin
    private EntityManager getEntityManager() {
        return entityManager;
    }

    @Produces
    private Logger getLogger(InjectionPoint ip) {
        return LoggerFactory.getLogger(ip.getBean().getBeanClass());
    }
}
