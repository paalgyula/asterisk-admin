package com.gwsystems.asteriskadmin.ejb;

import com.gwsystems.asteriskadmin.entity.SipBuddy;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Local
public interface SipBuddiesService {
    List<SipBuddy> getBuddies();

    SipBuddy findBuddyByCallerId(String callerId);

    long getBuddiesCount();

    void delBuddy(Long id);
}
