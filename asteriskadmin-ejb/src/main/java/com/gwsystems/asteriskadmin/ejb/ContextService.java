package com.gwsystems.asteriskadmin.ejb;

import com.gwsystems.asteriskadmin.entity.Extension;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Local
public interface ContextService {
    List<Extension> getExtensions();

    List<String> getExtensionNames();

    void removeExtension();

    long getExtensionCount();

    Extension addExtension(Extension extension);
}
