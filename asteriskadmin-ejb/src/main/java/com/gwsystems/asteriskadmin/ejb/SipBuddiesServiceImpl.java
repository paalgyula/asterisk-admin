package com.gwsystems.asteriskadmin.ejb;

import com.gwsystems.asteriskadmin.AsteriskAdmin;
import com.gwsystems.asteriskadmin.entity.SipBuddy;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Stateless
public class SipBuddiesServiceImpl implements SipBuddiesService {

    @Inject
    @AsteriskAdmin
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Override
    public List<SipBuddy> getBuddies() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SipBuddy> buddyQuery = cb.createQuery(SipBuddy.class);
        Root<SipBuddy> buddyRoot = buddyQuery.from(SipBuddy.class);
        buddyQuery.select(buddyRoot);

        return entityManager.createQuery(buddyQuery).getResultList();
    }

    @Override
    public SipBuddy findBuddyByCallerId(String callerId) {
        return null;
    }

    @Override
    public long getBuddiesCount() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> buddyCountQuery = cb.createQuery(Long.class);
        buddyCountQuery.select(cb.count(buddyCountQuery.from(SipBuddy.class)));
        return entityManager.createQuery(buddyCountQuery).getSingleResult();
    }

    @Override
    public void delBuddy(Long id) {
        SipBuddy buddy = entityManager.find(SipBuddy.class, id);
        if (buddy != null)
            entityManager.remove(buddy);
    }
}
