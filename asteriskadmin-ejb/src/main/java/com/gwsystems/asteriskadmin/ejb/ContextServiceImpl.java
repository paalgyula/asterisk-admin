package com.gwsystems.asteriskadmin.ejb;

import com.gwsystems.asteriskadmin.AsteriskAdmin;
import com.gwsystems.asteriskadmin.entity.Extension;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@Stateless
public class ContextServiceImpl implements ContextService {

    @Inject
    @AsteriskAdmin
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Override
    public List<Extension> getExtensions() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Extension> extensionCriteriaQuery = cb.createQuery(Extension.class);
        Root<Extension> extensionRoot = extensionCriteriaQuery.from(Extension.class);
        extensionCriteriaQuery.select(extensionRoot);

        return entityManager.createQuery(extensionCriteriaQuery).getResultList();
    }

    @Override
    public List<String> getExtensionNames() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<String> extensionCriteriaQuery = cb.createQuery(String.class);
        Root<Extension> extensionRoot = extensionCriteriaQuery.from(Extension.class);
        extensionCriteriaQuery.multiselect(extensionRoot.get("context")).groupBy(extensionRoot.get("context"));

        return entityManager.createQuery(extensionCriteriaQuery).getResultList();
    }

    @Override
    public void removeExtension() {
    }

    @Override
    public long getExtensionCount() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> buddyCountQuery = cb.createQuery(Long.class);
        buddyCountQuery.select(cb.count(buddyCountQuery.from(Extension.class)));
        return entityManager.createQuery(buddyCountQuery).getSingleResult();
    }

    @Override
    public Extension addExtension(Extension extension) {
        return entityManager.merge(extension);
    }
}
