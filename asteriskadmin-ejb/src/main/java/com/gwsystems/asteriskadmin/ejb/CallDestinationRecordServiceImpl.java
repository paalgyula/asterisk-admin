package com.gwsystems.asteriskadmin.ejb;

import com.gwsystems.asteriskadmin.AsteriskAdmin;
import com.gwsystems.asteriskadmin.entity.CallDestinationRecord;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2013.10.27.
 * Time: 14:33
 */
@Stateless(name = "CdrService")
public class CallDestinationRecordServiceImpl implements CallDestinationRecordService {

    @Inject
    @AsteriskAdmin
    private EntityManager entityManager;

    @Inject
    private Logger logger;

    @Override
    public List<CallDestinationRecord> getCdrForBuddy(String source, int limit, int start) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<CallDestinationRecord> query = cb.createQuery(CallDestinationRecord.class);
        Root<CallDestinationRecord> root = query.from(CallDestinationRecord.class);
        query.where(cb.equal(root.get("src"), source));
        query.orderBy(cb.desc(root.get("calldate")));
        query.select(root);

        return entityManager.createQuery(query)
                .setFirstResult(start)
                .setMaxResults(limit)
                .getResultList();

        /*return entityManager.createQuery( "select c from CallDestinationRecord c where c.clid = :callerId", CallDestinationRecord.class )
                .setParameter( "callerId", callerId )
                .setFirstResult(start)
                .setMaxResults( limit )
                .getResultList();*/
    }

    @Override
    public List<CallDestinationRecord> getCdrForBuddy(long sipBuddyId, int limit, int start) {
        return null;  // TODO: implement
    }

    @Override
    public List<CallDestinationRecord> getCdrForBuddy(long sipBuddyId, int limit, int start, String orderColumn) {
        return null;  // TODO: implement getCdrForBuddy method in class: com.gwsystems.asteriskadmin.ejb.CallDestinationRecordServiceImpl
    }

    @Override
    public List<CallDestinationRecord> getCdrGeneral(int limit, int start) {
        return entityManager.createQuery("select c from CallDestinationRecord c", CallDestinationRecord.class)
                .setFirstResult(start)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public List<CallDestinationRecord> getCdrGeneral(int limit, int start, String orderColumn) {
        return null;  // TODO: implement getCdrGeneral method in class: com.gwsystems.asteriskadmin.ejb.CallDestinationRecordServiceImpl
    }
}
