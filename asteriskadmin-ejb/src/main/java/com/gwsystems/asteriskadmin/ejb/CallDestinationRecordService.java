package com.gwsystems.asteriskadmin.ejb;

import com.gwsystems.asteriskadmin.entity.CallDestinationRecord;

import javax.ejb.Local;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2013.10.27.
 * Time: 14:32
 */
@Local
public interface CallDestinationRecordService {
    List<CallDestinationRecord> getCdrForBuddy(String source, int limit, int start);

    List<CallDestinationRecord> getCdrForBuddy(long sipBuddyId, int limit, int start);

    List<CallDestinationRecord> getCdrForBuddy(long sipBuddyId, int limit, int start, String orderColumn);

    List<CallDestinationRecord> getCdrGeneral(int limit, int start);

    List<CallDestinationRecord> getCdrGeneral(int limit, int start, String orderColumn);
}
