package com.gwsystems.asteriskadmin;

import java.lang.annotation.*;

/**
 * Created by paalgyula on 2013.10.23. with IntelliJ IDEA
 */
@javax.inject.Qualifier
@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AsteriskAdmin {

}
